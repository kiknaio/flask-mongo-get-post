from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'people'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/people'

mongo = PyMongo(app)

@app.route('/', methods=['GET'])
def get_people():
  people = mongo.db.people
  output = []
  for person in people.find():
    output.append({'firstname' : person['firstname'], 'lastname' : person['lastname']})
  return jsonify({'result' : output})

@app.route('/add-person', methods=['POST'])
def add_person():
  people = mongo.db.people
  firstname = request.json['firstname']
  lastname = request.json['lastname']

  person_id = people.insert({'firstname': firstname, 'lastname': lastname})
  new_person = people.find_one({'_id': person_id })

  output = {'firstname' : new_person['firstname'], 'lastname' : new_person['lastname']}
  return jsonify({'result' : output})

if __name__ == '__main__':
    app.run(debug=True)
